package maszyna;

import maszyna.gui.ObslugaBledow;
import maszyna.parser.MaszynaConfigParser;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppMain {
    public static void main(String[] args) {
        List<Farba> farby = new ArrayList<>();
        List<Pigment> pigmenty = new ArrayList<>();
        Maszyna sterownik = new Sterownik();

        try {
            new MaszynaConfigParser().czytajConfig(farby, pigmenty);
        } catch (IOException w) {
            new ObslugaBledow().WyswietlBladParsowania(w.getMessage());
        }

        SwingUtilities.invokeLater(() -> new maszyna.gui.MaszynaMain(farby, pigmenty, sterownik).initUI());
    }
}
