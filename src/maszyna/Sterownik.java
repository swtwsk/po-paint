package maszyna;

import maszyna.wyjatki.PrzekroczonoZakres;
import maszyna.wyjatki.ZlyPigment;

public class Sterownik implements Maszyna {
    private Farba mieszanaFarba;

    @Override
    public void rozpocznijMieszanie(Farba aktualnaFarba) {
        mieszanaFarba = aktualnaFarba.kopiuj();
    }

    @Override
    public void zakonczMieszanie() {
        mieszanaFarba = null;
    }

    @Override
    public boolean czyRozpoczetoMieszanie() {
        return (mieszanaFarba != null);
    }

    @Override
    public Farba dodajPigment(Pigment pigment) throws ZlyPigment, PrzekroczonoZakres {
        if(!pigment.getKolorPoczatkowy().equals(mieszanaFarba.getNazwa())) {
            throw new ZlyPigment();
        }

        mieszanaFarba.setNazwa(pigment.getKolorKoncowy());

        int nowaToksycznosc = pigment.ustawNowaToksycznosc(mieszanaFarba.getToksycznosc());
        if(nowaToksycznosc > 100) {
            throw new PrzekroczonoZakres("Za duża toksyczność");
        }
        else if (nowaToksycznosc < 0) {
            throw new PrzekroczonoZakres("Toksyczność poniżej zera");
        }
        mieszanaFarba.setToksycznosc(nowaToksycznosc);

        int nowaJakosc = pigment.ustawNowaJakosc(mieszanaFarba.getJakosc());
        if(nowaJakosc > 100) {
            throw new PrzekroczonoZakres("Za duża jakość");
        }
        else if (nowaJakosc < 0) {
            throw new PrzekroczonoZakres("Jakość poniżej zera");
        }
        mieszanaFarba.setJakosc(pigment.ustawNowaJakosc(mieszanaFarba.getJakosc()));

        return mieszanaFarba;
    }
}
