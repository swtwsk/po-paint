package maszyna;

import maszyna.wyjatki.PrzekroczonoZakres;
import maszyna.wyjatki.ZlyPigment;

public interface Maszyna {
    /**
     * Podaje farbę do wymieszania do maszyny.
     * @param aktualnaFarba farba do wymieszania
     */
    void rozpocznijMieszanie(Farba aktualnaFarba);

    /**
     * Kończy procedurę mieszania farby.
     */
    void zakonczMieszanie();

    /**
     * Zwraca informację, czy rozpoczęto już procedurę mieszania farby.
     * @return czy rozpoczęto mieszanie?
     */
    boolean czyRozpoczetoMieszanie();

    /**
     * Dodaje pigment do aktualnie mieszanej farby i zwraca nową, uzyskaną w ten sposób
     * @param pigment pigment dodawany do farby
     * @return nowa farba
     * @throws ZlyPigment jeżeli dodawany pigment wpływa na inną farbę
     * @throws PrzekroczonoZakres jeżeli poprzez dodanie pigmentu wartości nie mieszczą się w bezpiecznym zakresie
     */
    Farba dodajPigment(Pigment pigment) throws ZlyPigment, PrzekroczonoZakres;
}