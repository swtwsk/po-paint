package maszyna;

public class Pigment {
    private String nazwa;
    private String kolorPoczatkowy;
    private String kolorKoncowy;
    private String znakToksycznosci;
    private double wspToksycznosci;
    private String znakJakosci;
    private double wspJakosci;
    private boolean czyNazwaEdytowalna;

    public Pigment(String nazwa, String kolorPoczatkowy, String kolorKoncowy, String znakToksycznosci, double wspToksycznosci,
            String znakJakosci, double wspJakosci, boolean czyNazwaEdytowalna) {
        this.nazwa = nazwa;
        this.kolorKoncowy = kolorKoncowy;
        this.kolorPoczatkowy = kolorPoczatkowy;
        this.znakToksycznosci = znakToksycznosci;
        this.wspToksycznosci = wspToksycznosci;
        this.znakJakosci = znakJakosci;
        this.wspJakosci = wspJakosci;
        this.czyNazwaEdytowalna = czyNazwaEdytowalna;
    }

    public String getNazwa() {
        return nazwa;
    }
    public String getKolorPoczatkowy() {
        return kolorPoczatkowy;
    }
    public String getKolorKoncowy() {
        return kolorKoncowy;
    }
    public String getZnakToksycznosci() {
        return znakToksycznosci;
    }
    public double getWspToksycznosci() {
        return wspToksycznosci;
    }
    public String getZnakJakosci() {
        return znakJakosci;
    }
    public double getWspJakosci() {
        return wspJakosci;
    }
    public boolean isCzyNazwaEdytowalna() {
        return czyNazwaEdytowalna;
    }

    public void setKolorPoczatkowy(String kolorPoczatkowy) {
        this.kolorPoczatkowy = kolorPoczatkowy;
    }
    public void setKolorKoncowy(String kolorKoncowy) {
        this.kolorKoncowy = kolorKoncowy;
    }
    public void setZnakToksycznosci(String znakToksycznosci) {
        this.znakToksycznosci = znakToksycznosci;
    }
    public void setWspToksycznosci(double wspToksycznosci) {
        this.wspToksycznosci = wspToksycznosci;
    }
    public void setZnakJakosci(String znakJakosci) {
        this.znakJakosci = znakJakosci;
    }
    public void setWspJakosci(double wspJakosci) {
        this.wspJakosci = wspJakosci;
    }
    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int ustawNowaToksycznosc(int staraToksycznosc) {
        Double wynik;

        if(znakToksycznosci.equals("+")) {
            wynik = staraToksycznosc + wspToksycznosci;
            return wynik.intValue();
        }
        else if (znakToksycznosci.equals("-")) {
            wynik = staraToksycznosc - wspToksycznosci;
            return wynik.intValue();
        }
        else if (znakToksycznosci.equals("x")) {
            wynik = staraToksycznosc * wspToksycznosci;
            return wynik.intValue();
        }

        return 0;
    }

    public int ustawNowaJakosc(int staraJakosc) {
        Double wynik;

        if(znakJakosci.equals("+")) {
            wynik = staraJakosc + wspJakosci;
            return wynik.intValue();
        }
        else if (znakJakosci.equals("x")) {
            wynik = staraJakosc * wspJakosci;
            return wynik.intValue();
        }
        else if (znakJakosci.equals("-")) {
            wynik = staraJakosc - wspJakosci;
            return wynik.intValue();
        }

        return 0;
    }

    @Override
    public String toString() {
        return "nazwa: " + nazwa + ", kolor początkowy: " + kolorPoczatkowy + ", kolor końcowy: " + kolorKoncowy +
                ", toksyczność: " + znakToksycznosci + wspToksycznosci + ", jakość: " + znakJakosci + wspJakosci;
    }
}
