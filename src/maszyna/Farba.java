package maszyna;

public class Farba {
    private String nazwa;
    private int toksycznosc;
    private int jakosc;
    private boolean czyNazwaEdytowalna;

    public Farba(String nazwa, int toksycznosc, int jakosc, boolean czyNazwaEdytowalna) {
        this.nazwa = nazwa;
        this.toksycznosc = toksycznosc;
        this.jakosc = jakosc;
        this.czyNazwaEdytowalna = czyNazwaEdytowalna;
    }

    public Farba kopiuj() {
        return new Farba(nazwa, toksycznosc, jakosc, czyNazwaEdytowalna);
    }

    public String getNazwa() {
        return nazwa;
    }
    public int getToksycznosc() {
        return toksycznosc;
    }
    public int getJakosc() {
        return jakosc;
    }
    public boolean isCzyNazwaEdytowalna() {
        return czyNazwaEdytowalna;
    }

    public void setToksycznosc(int nowaToksycznosc) {
        toksycznosc = nowaToksycznosc;
    }
    public void setJakosc(int nowaJakosc) {
        jakosc = nowaJakosc;
    }
    public void setNazwa(String nowaNazwa) {
        nazwa = nowaNazwa;
    }

    @Override
    public String toString() {
        return "kolor: " + nazwa + ", toksyczność: " + toksycznosc + ", jakość: " + jakosc;
    }
}
