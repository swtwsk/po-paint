package maszyna.gui;

import maszyna.Pigment;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PigmentListSelectionListener implements ListSelectionListener {
    private JList farbyList;
    private JList pigmentList;
    private JTextField nazwaEditField;
    private JTextField toksycznoscEditField;
    private JTextField jakoscEditField;
    private JTextField kolorPoczatkowyTextField;
    private JTextField kolorPoczatkowyEditField;
    private JTextField kolorKoncowyTextField;
    private JTextField kolorKoncowyEditField;
    private JButton zatwierdzZmianyBtn;
    private JButton mieszajBtn;

    public PigmentListSelectionListener(JList farbyList, JList pigmentList, JTextField nazwaEditField, JTextField toksycznoscEditField,
                                        JTextField jakoscEditField, JTextField kolorPoczatkowyTextField, JTextField kolorPoczatkowyEditField,
                                        JTextField kolorKoncowyTextField, JTextField kolorKoncowyEditField, JButton zatwierdzZmianyBtn,
                                        JButton mieszajBtn) {
        this.farbyList = farbyList;
        this.pigmentList = pigmentList;
        this.nazwaEditField = nazwaEditField;
        this.toksycznoscEditField = toksycznoscEditField;
        this.jakoscEditField = jakoscEditField;
        this.kolorPoczatkowyTextField = kolorPoczatkowyTextField;
        this.kolorPoczatkowyEditField = kolorPoczatkowyEditField;
        this.kolorKoncowyTextField = kolorKoncowyTextField;
        this.kolorKoncowyEditField = kolorKoncowyEditField;
        this.zatwierdzZmianyBtn = zatwierdzZmianyBtn;
        this.mieszajBtn = mieszajBtn;
    }

    private void pokazParametryDoEdycji(boolean czyNazwaEdytowalna) {
        nazwaEditField.setEnabled(true);
        toksycznoscEditField.setEnabled(true);
        jakoscEditField.setEnabled(true);
        kolorPoczatkowyTextField.setEnabled(true);
        kolorPoczatkowyEditField.setEnabled(true);
        kolorKoncowyTextField.setEnabled(true);
        kolorKoncowyEditField.setEnabled(true);
        zatwierdzZmianyBtn.setEnabled(true);
        mieszajBtn.setEnabled(false);

        if (czyNazwaEdytowalna) {
            nazwaEditField.setEditable(true);
        } else {
            nazwaEditField.setEditable(false);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
        if (event.getValueIsAdjusting()) {
            {
                farbyList.clearSelection();

                pokazParametryDoEdycji(((Pigment) pigmentList.getSelectedValue()).isCzyNazwaEdytowalna());
                nazwaEditField.setText(((Pigment) pigmentList.getSelectedValue()).getNazwa());
                toksycznoscEditField.setText(((Pigment) pigmentList.getSelectedValue()).getZnakToksycznosci()
                        + ((Pigment) pigmentList.getSelectedValue()).getWspToksycznosci());
                jakoscEditField.setText(((Pigment) pigmentList.getSelectedValue()).getZnakJakosci()
                        + ((Pigment) pigmentList.getSelectedValue()).getWspJakosci());

                kolorPoczatkowyEditField.setText(((Pigment) pigmentList.getSelectedValue()).getKolorPoczatkowy());
                kolorKoncowyEditField.setText(((Pigment) pigmentList.getSelectedValue()).getKolorKoncowy());
            }
        }
    }
}
