package maszyna.gui;

import maszyna.*;
import maszyna.wyjatki.BlednaNazwa;
import maszyna.wyjatki.PrzekroczonoZakres;
import maszyna.wyjatki.ZlyPigment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Random;

public class MaszynaMain {

    /** ELEMENTY GUI */
    private JList farbyList;
    private JList pigmentList;
    private JPanel maszynaView;
    private JButton mieszajBtn;
    private JButton uzyjPigmentuBtn;
    private JTextField parametryTextField;
    private JButton dodajFarbeBtn;
    private JButton dodajPigmentBtn;
    private JTextField nazwaTextField;
    private JTextField nazwaEditField;
    private JTextField pigmentyTextField;
    private JTextField farbyTextField;
    private JTextField toksycznoscTextField;
    private JTextField toksycznoscEditField;
    private JTextField jakoscTextField;
    private JTextField jakoscEditField;
    private JTextField kolorPoczatkowyTextField;
    private JTextField kolorPoczatkowyEditField;
    private JTextField kolorKoncowyTextField;
    private JTextField kolorKoncowyEditField;
    private JButton zatwierdzZmianyBtn;
    private JTextField wyjscieKonsoli1;
    private JTextField wyjscieKonsoli2;

    private List<Farba> farby;
    private List<Pigment> pigmenty;
    private Maszyna sterownik;
    private ObslugaWyswietlania obslugaWyswietlania;
    private ObslugaBledow obslugaBledow;
    private EdycjaParametrow edycjaParametrow;

    private Random losuj = new Random();
    private int numerFarby; //numer, używany jeżeli losowanie spośród kolorów z listy dostępnych kolorów się nie powiedzie
    private int numerPigmentu;

    public MaszynaMain(List<Farba> farby, List<Pigment> pigmenty, Maszyna sterownik) {
        this.farby = farby;
        this.pigmenty = pigmenty;
        this.sterownik = sterownik;
        this.obslugaWyswietlania = new ObslugaWyswietlania(wyjscieKonsoli1, wyjscieKonsoli2);
        this.obslugaBledow = new ObslugaBledow();
        this.edycjaParametrow = new EdycjaParametrow(farbyList, pigmentList, nazwaEditField, toksycznoscEditField,
                jakoscEditField, kolorPoczatkowyEditField, kolorKoncowyEditField);
        this.numerFarby = 0;
        this.numerPigmentu = 0;

        farbyList.addListSelectionListener(new FarbyListSelectionListener(farbyList, pigmentList, nazwaEditField,
                toksycznoscEditField, jakoscEditField, kolorPoczatkowyTextField, kolorPoczatkowyEditField,
                kolorKoncowyTextField, kolorKoncowyEditField, zatwierdzZmianyBtn, mieszajBtn));
        pigmentList.addListSelectionListener(new PigmentListSelectionListener(farbyList, pigmentList, nazwaEditField,
                toksycznoscEditField, jakoscEditField, kolorPoczatkowyTextField, kolorPoczatkowyEditField,
                kolorKoncowyTextField, kolorKoncowyEditField, zatwierdzZmianyBtn, mieszajBtn));

        zatwierdzZmianyBtn.addActionListener(new zatwierdzZmianyIsClicked());
        dodajFarbeBtn.addActionListener(new dodajFarbeIsClicked());
        dodajPigmentBtn.addActionListener(new dodajPigmentIsClicked());
        mieszajBtn.addActionListener(new mieszajBtnIsClicked());
        uzyjPigmentuBtn.addActionListener(new uzyjPigmentuBtnIsClicked());
    }

    private void zakonczMieszanie() {
        if(sterownik.czyRozpoczetoMieszanie()) {
            sterownik.zakonczMieszanie();
            obslugaWyswietlania.wyswietlNaEkranKonsole("Koniec mieszania");
            uzyjPigmentuBtn.setEnabled(false);
            mieszajBtn.setEnabled(false);
        }
    }

    private class zatwierdzZmianyIsClicked implements ActionListener {
        private void wylaczEdycje() {
            nazwaEditField.setText("");
            nazwaEditField.setEnabled(false);

            toksycznoscEditField.setEnabled(false);
            toksycznoscEditField.setText("");

            jakoscEditField.setEnabled(false);
            jakoscEditField.setText("");

            kolorPoczatkowyEditField.setEnabled(false);
            kolorPoczatkowyEditField.setText("");

            kolorKoncowyEditField.setEnabled(false);
            kolorKoncowyEditField.setText("");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                edycjaParametrow.wykonajEdycje(farby, pigmenty);
                wylaczEdycje();
                farbyList.setListData(farby.toArray());
                pigmentList.setListData(pigmenty.toArray());
            } catch (PrzekroczonoZakres w) {
                obslugaBledow.WyswietlBlad("Przekroczono zakres", w.getTrescBledu());
            } catch (BlednaNazwa w) {
                obslugaBledow.WyswietlBlad("Błędny parametr", w.getTrescBledu());
            }

            zakonczMieszanie();
            mieszajBtn.setEnabled(false);
        }
    }

    private class dodajFarbeIsClicked implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean czyKolorZnaleziony = false;
            ListaKolorow nowyKolor;

            for(ListaKolorow kolor : ListaKolorow.values()) {
                nowyKolor = kolor;
                czyKolorZnaleziony = true;

                for(Farba f : farby) {
                    if(f.getNazwa().equals(kolor.toString())) {
                        czyKolorZnaleziony = false;
                        break;
                    }
                }

                if(czyKolorZnaleziony) {
                    dodajFarbe(new Farba(nowyKolor.toString(), losuj.nextInt(101), losuj.nextInt(101), true));
                    break;
                }
            }

            if(!czyKolorZnaleziony) {
                System.out.println("BRAK DOSTEPNYCH KOLOROW!");
                dodajFarbe(new Farba("nowa-farba" + numerFarby++, losuj.nextInt(101), losuj.nextInt(101), true));
            }

            zakonczMieszanie();
            mieszajBtn.setEnabled(false);
        }
    }

    private class dodajPigmentIsClicked implements ActionListener {
        private String nowaNazwa() {
            int ileKombinacjiPozostalo = 1679616;

            while(true) {
                if(ileKombinacjiPozostalo-- <= 0) {
                    System.out.println("BRAK DOSTEPNYCH PIGMENTÓW!");
                    return "nowy-pigment" + numerPigmentu++;
                }

                boolean czyPigmentUnikalny = true;
                String alfabet = "0123456789ABCDEFGHIJKLMNOPRQSTUVWXYZ";

                StringBuilder nowyPigment = new StringBuilder();
                for (int i = 0; i < 4; i++) {
                    nowyPigment.append(alfabet.charAt(losuj.nextInt(alfabet.length())));
                }
                nowyPigment.append("IP0");
                nowyPigment.reverse();

                for(Pigment p : pigmenty) {
                    if(p.getNazwa().equals(nowyPigment.toString())) {
                        czyPigmentUnikalny = false;
                        break;
                    }
                }

                if(czyPigmentUnikalny) {
                    return nowyPigment.toString();
                }
            }
        }

        private String losujKolorPoczatkowy() {
            return ListaKolorow.losowyKolor().toString();
        }
        private String losujKolorKoncowy(String nowyKolorPoczatkowy) {
            String nowyKolor;

            do {
                nowyKolor = ListaKolorow.losowyKolor().toString();
            } while(nowyKolor.equals(nowyKolorPoczatkowy));

            return nowyKolor;
        }

        private String losujZnak() {
            String alfabet = "+-x";
            int wylosowana = losuj.nextInt(alfabet.length());
            return alfabet.substring(wylosowana, wylosowana + 1);
        }

        private double losujWspolczynnik() {
            double wylosowana = 100 * losuj.nextDouble();
            wylosowana = Math.round(wylosowana * 10);
            return wylosowana / 10;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String nowyKolorPoczatkowy = losujKolorPoczatkowy();
            dodajPigment(new Pigment(nowaNazwa(), nowyKolorPoczatkowy, losujKolorKoncowy(nowyKolorPoczatkowy),
                    losujZnak(), losujWspolczynnik(), losujZnak(), losujWspolczynnik(), true));
            zakonczMieszanie();
        }
    }

    private class mieszajBtnIsClicked implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!farbyList.isSelectionEmpty()) {
                Farba wybranaFarba = (Farba) farbyList.getSelectedValue();
                sterownik.rozpocznijMieszanie(wybranaFarba);

                if(sterownik.czyRozpoczetoMieszanie()) {
                    uzyjPigmentuBtn.setEnabled(true);
                }
                obslugaWyswietlania.wyswietlNaEkranKonsole("Zaczynam mieszanie... Farba - " + wybranaFarba.toString());
            }
        }
    }

    private class uzyjPigmentuBtnIsClicked implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(!pigmentList.isSelectionEmpty()) {
                Pigment wybranyPigment = (Pigment) pigmentList.getSelectedValue();
                try {
                    Farba nowaFarba = sterownik.dodajPigment(wybranyPigment);
                    String daneNowejFarby = "Farba - " + nowaFarba.toString();
                    String danePigmentu = "Dodano: " + wybranyPigment.toString();
                    obslugaWyswietlania.wyswietlNaEkranKonsole(danePigmentu, daneNowejFarby);
                } catch (ZlyPigment w) {
                    obslugaBledow.WyswietlBlad("Błędny pigment", "Podano niepoprawny pigment dla mieszanej farby");
                } catch (PrzekroczonoZakres w) {
                    obslugaBledow.WyswietlBlad("Przekroczono zakres", w.getTrescBledu());
                }
            }
            else {
                obslugaBledow.WyswietlBlad("Brak pigmentu", "Nie wybrano pigmentu");
            }
        }
    }

    private void ustawListeFarb() {
        farbyList.setListData(farby.toArray());
        farbyList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value,
                                                          int index, boolean isSelected, boolean cellHasFocus) {
                Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (renderer instanceof JLabel && value instanceof Farba) {
                    ((JLabel) renderer).setText(((Farba) value).getNazwa());
                }
                return renderer;
            }
        });
    }

    private void ustawListePigmentow() {
        pigmentList.setListData(pigmenty.toArray());
        pigmentList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> list, Object value,
                                                          int index, boolean isSelected, boolean cellHasFocus) {
                Component renderer = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (renderer instanceof JLabel && value instanceof Pigment) {
                    ((JLabel) renderer).setText(((Pigment) value).getNazwa());
                }
                return renderer;
            }
        });
    }

    private void schowajParametry() {
        nazwaEditField.setEnabled(false);
        toksycznoscEditField.setEnabled(false);
        jakoscEditField.setEnabled(false);
        kolorPoczatkowyTextField.setEnabled(false);
        kolorPoczatkowyEditField.setEnabled(false);
        kolorKoncowyTextField.setEnabled(false);
        kolorKoncowyEditField.setEnabled(false);
        zatwierdzZmianyBtn.setEnabled(false);
    }
    private void schowajPrzyciski() {
        mieszajBtn.setEnabled(false);
        uzyjPigmentuBtn.setEnabled(false);
    }

    private void dodajFarbe(Farba farba) {
        farby.add(farba);
        pigmentList.clearSelection();
        farbyList.setListData(farby.toArray());
    }
    private void dodajPigment(Pigment pigment) {
        pigmenty.add(pigment);
        pigmentList.clearSelection();
        pigmentList.setListData(pigmenty.toArray());
    }

    public void initUI() {
        JFrame frame = new JFrame("Sterownik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        farbyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pigmentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        wyjscieKonsoli1.setBorder(BorderFactory.createEmptyBorder());
        wyjscieKonsoli2.setBorder(BorderFactory.createEmptyBorder());

        schowajParametry();
        schowajPrzyciski();

        ustawListeFarb();
        ustawListePigmentow();

        frame.setContentPane(maszynaView);
        frame.pack();
        frame.setVisible(true);
    }
}
