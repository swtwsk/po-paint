package maszyna.gui;

import maszyna.Farba;
import maszyna.Pigment;
import maszyna.wyjatki.BlednaNazwa;
import maszyna.wyjatki.PrzekroczonoZakres;

import javax.swing.*;
import java.util.List;

public class EdycjaParametrow {
    private JList farbyList;
    private JList pigmentList;
    private JTextField nazwaEditField;
    private JTextField toksycznoscEditField;
    private JTextField jakoscEditField;
    private JTextField kolorPoczatkowyEditField;
    private JTextField kolorKoncowyEditField;

    private static final String FARBA_REGEX_MATCH_PATTERN =
            "[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ][a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]*[\\-]?[a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]*";
    private static final String PIGMENT_REGEX_MATCH_PATTERN = "[a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]+";
    private static final String ZNAK_REGEX_MATCH_PATTERN = "[+\\-x]";
    private static final String INT_REGEX_MATCH_PATTERN = "[\\-]?[0-9]+";
    private static final String DOUBLE_REGEX_MATCH_PATTERN = "[0-9]+([.][0-9]*)?([E][+\\-][0-9]+)?";

    protected EdycjaParametrow(JList farbyList, JList pigmentList, JTextField nazwaEditField, JTextField toksycznoscEditField,
                            JTextField jakoscEditField, JTextField kolorPoczatkowyEditField, JTextField kolorKoncowyEditField) {
        this.farbyList = farbyList;
        this.pigmentList = pigmentList;
        this.nazwaEditField = nazwaEditField;
        this.toksycznoscEditField = toksycznoscEditField;
        this.jakoscEditField = jakoscEditField;
        this.kolorPoczatkowyEditField = kolorPoczatkowyEditField;
        this.kolorKoncowyEditField = kolorKoncowyEditField;
    }

    private void sprawdzNazweFarby(String nowaNazwa, String poprzedniaNazwa, List<Farba> farby) throws BlednaNazwa {
        if(!nowaNazwa.equals(poprzedniaNazwa)) {
            for (Farba f : farby) {
                if (f.getNazwa().equals(nowaNazwa)) {
                    throw new BlednaNazwa("Nazwa farby w użyciu!");
                }
            }
        }
    }

    private void ustawParametryFarby(String jakosc, String toksycznosc, List<Farba> farby) throws PrzekroczonoZakres, BlednaNazwa {
        Farba wybranaFarba = (Farba) farbyList.getSelectedValue();

        if (!nazwaEditField.getText().matches(FARBA_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Błędna nazwa/kolor farby");
        }
        sprawdzNazweFarby(nazwaEditField.getText(), wybranaFarba.getNazwa(), farby);

        if (!toksycznosc.matches(INT_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Wartość toksyczności nie jest liczbą całkowitą");
        }
        int nowaToksycznosc = Integer.parseInt(toksycznosc);
        if (nowaToksycznosc > 100) {
            throw new PrzekroczonoZakres("Za duża toksyczność (maksymalna wartość to 100)");
        } else if (nowaToksycznosc < 0) {
            throw new PrzekroczonoZakres("Toksyczność poniżej zera");
        }

        if (!jakosc.matches(INT_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Wartość jakości nie jest liczbą całkowitą");
        }
        int nowaJakosc = Integer.parseInt(jakosc);
        if (nowaJakosc > 100) {
            throw new PrzekroczonoZakres("Za duża jakość (maksymalna wartość to 100)");
        } else if (nowaJakosc < 0) {
            throw new PrzekroczonoZakres("Jakość poniżej zera");
        }

        wybranaFarba.setJakosc(Integer.parseInt(jakosc));
        wybranaFarba.setToksycznosc(nowaToksycznosc);
        wybranaFarba.setNazwa(nazwaEditField.getText());
    }

    private void sprawdzNazwePigmentu(String nowaNazwa, String poprzedniaNazwa, List<Pigment> pigmenty) throws BlednaNazwa {
        if(!nowaNazwa.equals(poprzedniaNazwa)) {
            for (Pigment p : pigmenty) {
                if (p.getNazwa().equals(nowaNazwa)) {
                    throw new BlednaNazwa("Nazwa pigmentu w użyciu!");
                }
            }
        }
    }

    private void ustawParametryPigmentu(String jakosc, String toksycznosc, List<Pigment> pigmenty) throws PrzekroczonoZakres, BlednaNazwa {
        Pigment wybranyPigment = (Pigment) pigmentList.getSelectedValue();

        if (!nazwaEditField.getText().matches(PIGMENT_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Błędna nazwa pigmentu");
        }
        sprawdzNazwePigmentu(nazwaEditField.getText(), wybranyPigment.getNazwa(), pigmenty);

        String nowyZnakToksycznosci = toksycznosc.substring(0, 1);
        if (!nowyZnakToksycznosci.matches(ZNAK_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Błędny znak toksyczności");
        }

        toksycznosc = toksycznosc.substring(1, toksycznosc.length());
        if (!toksycznosc.matches(DOUBLE_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Współczynnik toksyczności nie jest liczbą zmiennoprzecinkową");
        }
        double nowyWspToksycznosci = Double.parseDouble(toksycznosc);
        if (nowyWspToksycznosci > 100) {
            throw new PrzekroczonoZakres("Za duży współczynnik toksyczności (maksymalna wartość to 100)");
        } else if (nowyWspToksycznosci < 0) {
            throw new PrzekroczonoZakres("Współczynnik toksyczności poniżej zera");
        }

        String nowyZnakJakosci = jakosc.substring(0, 1);
        if (!nowyZnakJakosci.matches(ZNAK_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Błędny znak jakości");
        }

        jakosc = jakosc.substring(1, jakosc.length());
        if (!jakosc.matches(DOUBLE_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Współczynnik jakości nie jest liczbą zmiennoprzecinkową");
        }
        double nowyWspJakosci = Double.parseDouble(jakosc);
        if (nowyWspJakosci > 100) {
            throw new PrzekroczonoZakres("Za duży współczynnik jakości (maksymalna wartość to 100)");
        } else if (nowyWspJakosci < 0) {
            throw new PrzekroczonoZakres("Współczynnik jakości poniżej zera");
        }

        if (!kolorPoczatkowyEditField.getText().matches(FARBA_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Błędna nazwa/kolor farby początkowej");
        }
        if (!kolorKoncowyEditField.getText().matches(FARBA_REGEX_MATCH_PATTERN)) {
            throw new BlednaNazwa("Błędna nazwa/kolor farby końcowej");
        }

        wybranyPigment.setKolorPoczatkowy(kolorPoczatkowyEditField.getText());
        wybranyPigment.setKolorKoncowy(kolorKoncowyEditField.getText());
        wybranyPigment.setNazwa(nazwaEditField.getText());

        wybranyPigment.setZnakJakosci(nowyZnakJakosci);
        wybranyPigment.setWspJakosci(nowyWspJakosci);

        wybranyPigment.setZnakToksycznosci(nowyZnakToksycznosci);
        wybranyPigment.setWspToksycznosci(nowyWspToksycznosci);
    }

    protected void wykonajEdycje(List<Farba> farby, List<Pigment> pigmenty) throws PrzekroczonoZakres, BlednaNazwa {
        String jakosc = jakoscEditField.getText();
        String toksycznosc = toksycznoscEditField.getText();

        if(!farbyList.isSelectionEmpty()) {
            ustawParametryFarby(jakosc, toksycznosc, farby);
        }
        else if(!pigmentList.isSelectionEmpty()) {
            ustawParametryPigmentu(jakosc, toksycznosc, pigmenty);
        }
    }
}
