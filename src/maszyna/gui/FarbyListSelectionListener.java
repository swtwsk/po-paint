package maszyna.gui;

import maszyna.Farba;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class FarbyListSelectionListener implements ListSelectionListener {
    private JList farbyList;
    private JList pigmentList;
    private JTextField nazwaEditField;
    private JTextField toksycznoscEditField;
    private JTextField jakoscEditField;
    private JTextField kolorPoczatkowyTextField;
    private JTextField kolorPoczatkowyEditField;
    private JTextField kolorKoncowyTextField;
    private JTextField kolorKoncowyEditField;
    private JButton zatwierdzZmianyBtn;
    private JButton mieszajBtn;

    public FarbyListSelectionListener(JList farbyList, JList pigmentList, JTextField nazwaEditField, JTextField toksycznoscEditField,
                                      JTextField jakoscEditField, JTextField kolorPoczatkowyTextField, JTextField kolorPoczatkowyEditField,
                                      JTextField kolorKoncowyTextField, JTextField kolorKoncowyEditField, JButton zatwierdzZmianyBtn,
                                      JButton mieszajBtn) {
        this.farbyList = farbyList;
        this.pigmentList = pigmentList;
        this.nazwaEditField = nazwaEditField;
        this.toksycznoscEditField = toksycznoscEditField;
        this.jakoscEditField = jakoscEditField;
        this.kolorPoczatkowyTextField = kolorPoczatkowyTextField;
        this.kolorPoczatkowyEditField = kolorPoczatkowyEditField;
        this.kolorKoncowyTextField = kolorKoncowyTextField;
        this.kolorKoncowyEditField = kolorKoncowyEditField;
        this.zatwierdzZmianyBtn = zatwierdzZmianyBtn;
        this.mieszajBtn = mieszajBtn;
    }

    private void pokazParametryDoEdycji(boolean czyNazwaEdytowalna) {
        nazwaEditField.setEnabled(true);
        toksycznoscEditField.setEnabled(true);
        jakoscEditField.setEnabled(true);

        kolorPoczatkowyEditField.setText("");
        kolorPoczatkowyTextField.setEnabled(false);
        kolorPoczatkowyEditField.setEnabled(false);
        kolorKoncowyEditField.setText("");
        kolorKoncowyTextField.setEnabled(false);
        kolorKoncowyEditField.setEnabled(false);

        zatwierdzZmianyBtn.setEnabled(true);
        mieszajBtn.setEnabled(true);

        if(czyNazwaEdytowalna) {
            nazwaEditField.setEditable(true);
        }
        else {
            nazwaEditField.setEditable(false);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
        if (event.getValueIsAdjusting()) {
            pigmentList.clearSelection();

            pokazParametryDoEdycji(((Farba) farbyList.getSelectedValue()).isCzyNazwaEdytowalna());
            nazwaEditField.setText(((Farba) farbyList.getSelectedValue()).getNazwa());
            toksycznoscEditField.setText(((Farba) farbyList.getSelectedValue()).getToksycznosc() + "");
            jakoscEditField.setText(((Farba) farbyList.getSelectedValue()).getJakosc() + "");
        }
    }
}
