package maszyna.gui;

import javax.swing.*;

public class ObslugaWyswietlania {
    private JTextField wyjscieKonsoli1;
    private JTextField wyjscieKonsoli2;

    ObslugaWyswietlania(JTextField wyjscieKonsoli1, JTextField wyjscieKonsoli2) {
        this.wyjscieKonsoli1 = wyjscieKonsoli1;
        this.wyjscieKonsoli2 = wyjscieKonsoli2;
    }

    public void wyswietlNaEkranKonsole(String wiadomosc) {
        System.out.println(wiadomosc);
        wyjscieKonsoli1.setText("");
        wyjscieKonsoli2.setText(wiadomosc);
    }
    public void wyswietlNaEkranKonsole(String pierwszaWiadomosc, String drugaWiadomosc) {
        System.out.println(pierwszaWiadomosc);
        wyjscieKonsoli1.setText(pierwszaWiadomosc);

        System.out.println(drugaWiadomosc);
        wyjscieKonsoli2.setText(drugaWiadomosc);
    }
}
