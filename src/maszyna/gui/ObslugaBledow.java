package maszyna.gui;

import javax.swing.*;

public class ObslugaBledow {
    public void WyswietlBladParsowania(String trescBledu) {
        System.out.println("ERROR: " + trescBledu);
        int option = JOptionPane.showOptionDialog(new JFrame(), trescBledu, "Błąd obsługi plików",
                JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, null, null);
        if (option == JOptionPane.OK_OPTION) {
            System.exit(1);
        }
    }

    public void WyswietlBlad(String tytulBledu, String trescBledu) {
        System.out.println("ERROR: " + trescBledu);
        JOptionPane.showMessageDialog(new JFrame(), trescBledu, tytulBledu, JOptionPane.ERROR_MESSAGE);
    }
}
