package maszyna.parser;
import maszyna.Farba;
import maszyna.Pigment;

import java.io.*;
import java.util.List;

public class MaszynaConfigParser {
    private static final String SCIEZKA_DO_PLIKU = "maszyna.conf";
    private static final String NAZWA_STEROWNIKA_REGEX_MATCH_PATTERN = "([a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]+[.])*[a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]+";

    public void czytajConfig(List<Farba> farby, List<Pigment> pigmenty) throws IOException {
        String sciezkaDoFarb;
        String sciezkaDoPigmentow;
        String nazwaSterownika;

        String wierszPliku;

        FileReader config = new FileReader(SCIEZKA_DO_PLIKU);
        BufferedReader reader = new BufferedReader(config);

        for(int i = 0; i < 3; i++) {
            wierszPliku = reader.readLine();

            if (wierszPliku == null) {
                throw new IOException("Błędny plik konfiguracyjny maszyny");
            }
            else {
                if(i == 0) {
                    sciezkaDoFarb = wierszPliku;
                    new FarbyConfigParser(sciezkaDoFarb).czytajConfig(farby);
                }
                else if (i == 1) {
                    sciezkaDoPigmentow = wierszPliku;
                    new PigmentyConfigParser(sciezkaDoPigmentow).czytajConfig(pigmenty);
                }
                else {
                    nazwaSterownika = wierszPliku;
                    if(!nazwaSterownika.matches(NAZWA_STEROWNIKA_REGEX_MATCH_PATTERN)) {
                        throw new IOException("Błędna nazwa klasy obsługującej sterownik");
                    }
                }
            }
        }

        wierszPliku = reader.readLine();
        if(wierszPliku != null) {
            throw new IOException("Błędny plik konfiguracyjny maszyny");
        }

        reader.close();
    }
}
