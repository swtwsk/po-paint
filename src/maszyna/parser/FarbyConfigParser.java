package maszyna.parser;

import maszyna.Farba;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class FarbyConfigParser {
    private String SCIEZKA_DO_PLIKU;
    private static final String REGEX_MATCH_PATTERN =
            "^[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ][a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]*[\\-]?[a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]*[ ]\\d+[ ]\\d+$";
    private static final String REGEX_SPLIT_PATTERN = "\\s";

    protected FarbyConfigParser(String SCIEZKA_DO_PLIKU) {
        this.SCIEZKA_DO_PLIKU = SCIEZKA_DO_PLIKU;
    }

    protected void czytajConfig(List<Farba> farby) throws IOException {
        FileReader config = new FileReader(SCIEZKA_DO_PLIKU);
        BufferedReader reader = new BufferedReader(config);
        String wierszPliku;

        do {
            wierszPliku = reader.readLine();

            if(wierszPliku == null) {
                break;
            }

            if(!wierszPliku.matches(REGEX_MATCH_PATTERN)) {
                throw new IOException("Błędny plik konfiguracyjny farb");
            }

            String[] dane = wierszPliku.split(REGEX_SPLIT_PATTERN);

            for(Farba f : farby) {
                if(f.getNazwa().equals(dane[0])) {
                    throw new IOException("Błędny plik konfiguracyjny farb");
                }
            }

            farby.add(new Farba(dane[0], Integer.parseInt(dane[1]), Integer.parseInt(dane[2]), false));
        } while(true);
    }
}
