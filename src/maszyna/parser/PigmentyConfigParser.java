package maszyna.parser;

import maszyna.Pigment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class PigmentyConfigParser {
    private String SCIEZKA_DO_PLIKU;
    private static final String REGEX_KOLOR_PATTERN =
            "[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ][a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]*[\\-]?[a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]*";
    private static final String REGEX_WSP_PATTERN = "[+\\-x][0-9]+[.]?[0-9]*";
    private static final String REGEX_MATCH_PATTERN =
            "[a-zA-Z0-9żźćńółęąśŻŹĆĄŚĘŁÓŃ]+[ ]" + REGEX_KOLOR_PATTERN + "[ ]" + REGEX_KOLOR_PATTERN + "[ ]"
                    + REGEX_WSP_PATTERN + "[ ]" + REGEX_WSP_PATTERN + "$";
    private static final String REGEX_SPLIT_PATTERN = "\\s";

    protected PigmentyConfigParser(String SCIEZKA_DO_PLIKU) {
        this.SCIEZKA_DO_PLIKU = SCIEZKA_DO_PLIKU;
    }

    protected void czytajConfig(List<Pigment> pigmenty) throws IOException {
        FileReader config = new FileReader(SCIEZKA_DO_PLIKU);
        BufferedReader reader = new BufferedReader(config);
        String wierszPliku;

        do {
            wierszPliku = reader.readLine();

            if(wierszPliku == null) {
                break;
            }

            if(!wierszPliku.matches(REGEX_MATCH_PATTERN)) {
                throw new IOException("Błędny plik konfiguracyjny pigmentów");
            }

            String[] dane = wierszPliku.split(REGEX_SPLIT_PATTERN);
            String znakToksycznosci = dane[3].substring(0, 1);
            double wspToksycznosci = Double.parseDouble(dane[3].substring(1, dane[3].length()));
            String znakJakosci = dane[4].substring(0, 1);
            double wspJakosci = Double.parseDouble(dane[4].substring(1, dane[4].length()));

            for(Pigment p : pigmenty) {
                if(p.getNazwa().equals(dane[0])) {
                    throw new IOException("Błędny plik konfiguracyjny pigmentów");
                }
            }

            pigmenty.add(new Pigment(dane[0], dane[1], dane[2], znakToksycznosci, wspToksycznosci, znakJakosci,
                    wspJakosci, false));
        } while(true);
    }
}
