package maszyna.wyjatki;

public class PrzekroczonoZakres extends Exception {
    private String trescBledu;

    public PrzekroczonoZakres(String trescBledu) {
        this.trescBledu = trescBledu;
    }

    public String getTrescBledu() {
        return trescBledu;
    }
}
