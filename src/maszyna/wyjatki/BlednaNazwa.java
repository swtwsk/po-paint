package maszyna.wyjatki;

public class BlednaNazwa extends Exception {
    private String trescBledu;

    public BlednaNazwa(String trescBledu) {
        this.trescBledu = trescBledu;
    }

    public String getTrescBledu() {
        return trescBledu;
    }
}
